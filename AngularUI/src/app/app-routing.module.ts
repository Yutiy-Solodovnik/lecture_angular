import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserComponent } from './user/user.component';
import { EditUserComponent } from './user/edit-user/edit-user.component';
import { ExitEditGuard } from './guards/guard';
import { EditProjectComponent } from './project/edit-project/edit-project.component';
import { ProjectComponent } from './project/project.component';
import { TeamComponent } from './team/team.component';
import { EditTeamComponent } from './team/edit-team/edit-team.component';
import { EditTaskComponent } from './task/edit-task/edit-task.component';
import { ShowTasksComponent } from './task/show-tasks/show-tasks.component';

const routes: Routes = [
  {
    path: "Users",
    component: UserComponent
  },
  {
    path: "EditUser",
    component: EditUserComponent,
    canDeactivate: [ExitEditGuard]
  },
  {
    path: "Teams",
    component: TeamComponent
  },
  {
    path: "EditTeam",
    component: EditTeamComponent,
    canDeactivate: [ExitEditGuard]
  },
  {
    path: "Projects",
    component: ProjectComponent
  },
  {
    path: "EditProject",
    component: EditProjectComponent,
    canDeactivate: [ExitEditGuard]
  },
  {
    path: "Tasks",
    component: ShowTasksComponent
  },
  {
    path: "EditTask",
    component: EditTaskComponent,
    canDeactivate: [ExitEditGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
