import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { UserService } from './services/user.service';
import { ProjectService } from './services/project.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ShowUsersComponent } from './user/show-users/show-users.component';
import { EditUserComponent } from './user/edit-user/edit-user.component';
import { DateFormatPipe } from './models/pipeDate';
import { ExitEditGuard } from './guards/guard';
import { ShowProjectsComponent } from './project/show-projects/show-projects.component';
import { EditProjectComponent } from './project/edit-project/edit-project.component';
import { ProjectComponent } from './project/project.component';
import { TeamComponent } from './team/team.component';
import { ShowTeamsComponent } from './team/show-teams/show-teams.component';
import { EditTeamComponent } from './team/edit-team/edit-team.component';
import { TaskComponent } from './task/task.component';
import { EditTaskComponent } from './task/edit-task/edit-task.component';
import { ShowTasksComponent } from './task/show-tasks/show-tasks.component';
import { TaskService } from './services/task.service';
import { TeamService } from './services/team.service';
import { StateDirective } from './directives/state.directive';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    ShowUsersComponent,
    EditUserComponent,
    DateFormatPipe,
    ShowProjectsComponent,
    EditProjectComponent,
    ProjectComponent,
    TeamComponent,
    ShowTeamsComponent,
    EditTeamComponent,
    TaskComponent,
    EditTaskComponent,
    ShowTasksComponent,
    StateDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [UserService, ProjectService, TeamService, TaskService, ExitEditGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
