import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
    selector: "[visual-state]"
})
export class StateDirective {
    constructor(private elementRef: ElementRef) { }

    @HostListener("DOMCharacterDataModified") async onMouseEnnter() {
        this.elementRef.nativeElement.style = "font-size: 1.5em;"
        await this.setState(this.elementRef.nativeElement.innerHTML);
        this.elementRef.nativeElement.innerHTML = this.content;
    }

    content: string = "";

    setState(stateStatus: string) {
        switch (stateStatus) {
            case "0":
                this.content = "&#128545;";
                break;
            case "1":
                this.content = "&#128543;";
                break;
            case "2":
                this.content = "&#128528;";
                break;
            case "3":
                this.content = "&#128541;";
                break;
        }
    }
}