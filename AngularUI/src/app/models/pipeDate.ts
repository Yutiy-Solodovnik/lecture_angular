import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "format"
})
export class DateFormatPipe implements PipeTransform {
  transform(value: Date): any {
    return new Date(value).toLocaleString("uk-UA", {
      day: "numeric",
      month: "long",
      year: "numeric",
    })
      .toString()
      .slice(0, -3);
  }
}