export class Project {
    constructor(
        public id: number,
        public authorId: number,
        public teamId: number,
        public name: string,
        public description: string,
        public deadline: Date,
        public createdAt: Date,
    ) { }
}