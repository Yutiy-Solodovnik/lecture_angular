export class Task {
    constructor(
        public id: number,
        public projectId: number,
        public performerId: number,
        public name: string,
        public description: string,
        public state: number,
        public createdAt: Date,
        public finishedAt: Date
    ) { }
}