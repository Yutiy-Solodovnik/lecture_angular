export class Team {
    constructor(
        public id: number,
        public createdAt: Date,
        public name?: string,
    ) { }
}