export class User {
    constructor(
        public id: number,
        public registeredAt: Date,
        public birthDay: Date,
        public teamId: number,
        public firstName: string,
        public lastName: string,
        public email: string
    ) { }
}