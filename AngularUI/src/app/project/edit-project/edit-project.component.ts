import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { Project } from '../../models/project';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ComponentCanDeactivate } from 'src/app/guards/guard';
import { Observable, Subscription } from "rxjs";
import { Router } from '@angular/router';
import { formatDate } from '@angular/common'

@Component({
  selector: 'app-edit-project',
  templateUrl: './edit-project.component.html',
  styleUrls: ['./edit-project.component.css']
})
export class EditProjectComponent implements OnInit, ComponentCanDeactivate, OnDestroy {

  constructor(private projectService: ProjectService, private router: Router) {
    this.project = this.router?.getCurrentNavigation()?.extras.state;
    this.modalTitle = this.project?.id == 0 ? "Add project" : "Update project";
    console.log(this.project);
  }

  ProjectId: number = 0;
  ProjectName: string = "";
  projectForm: FormGroup = new FormGroup({});
  modalTitle: string = "";
  saved: boolean = false;
  private subscriptioions = new Subscription();

  @Input() project: any = new Project(0, 0, 0, "", "", new Date(), new Date());
  ngOnInit(): void {

    this.ProjectId = this.project?.id;
    this.projectForm = new FormGroup({
      authorId: new FormControl(this.project?.authorId, [
        Validators.required,
        Validators.minLength(1),
        Validators.pattern("[1-9]+")]),
      teamId: new FormControl(this.project?.teamId, [
        Validators.required,
        Validators.minLength(1),
        Validators.pattern("[1-9]+")]
      ),
      name: new FormControl(this.project?.name, [
        Validators.required,
        Validators.minLength(2),
        Validators.pattern("[a-zA-Z ]+$")]),
      description: new FormControl(this.project?.description, [
        Validators.required,
        Validators.minLength(2),
        Validators.pattern("[a-zA-Z ]+$")]),
      deadline: new FormControl(formatDate(new Date(this.project?.deadline), 'yyyy-MM-dd', 'en'), [
        Validators.required]),
      createdAt: new FormControl(formatDate(new Date(this.project?.createdAt), 'yyyy-MM-dd', 'en'), [
        Validators.required]),
    });
  }

  canDeactivate(): boolean | Observable<boolean> {

    if (!this.saved) {
      return confirm("You have unsaved data. Do you want to exit?");
    }
    else {
      return true;
    }
  }

  save() {
    this.saved = true;
  }

  addProject() {
    if (this.projectForm.valid) {
      let project: Project = {
        id: this.ProjectId,
        authorId: this.projectForm.controls["authorId"].value,
        teamId: this.projectForm.controls["teamId"].value,
        name: this.projectForm.controls["name"].value,
        description: this.projectForm.controls["description"].value,
        deadline: this.projectForm.controls["deadline"].value,
        createdAt: this.projectForm.controls["createdAt"].value
      }
      this.subscriptioions = this.projectService.createProject(project).subscribe(res => alert(res.toString()));
      this.save();
      this.closeClick();
    }
  }

  updateProject() {
    let project: Project = {
      id: this.ProjectId,
      authorId: this.projectForm.controls["authorId"].value,
      teamId: this.projectForm.controls["teamId"].value,
      name: this.projectForm.controls["name"].value,
      description: this.projectForm.controls["description"].value,
      deadline: this.projectForm.controls["deadline"].value,
      createdAt: this.projectForm.controls["createdAt"].value
    }
    this.subscriptioions = this.projectService.updateProject(project).subscribe(res => alert(res.toString()));
    this.save();
    this.closeClick();
  }

  closeClick() {
    this.router.navigateByUrl("Projects");
  }

  ngOnDestroy(): void {
    this.subscriptioions.unsubscribe()
  }
}
