import { Component, OnDestroy, OnInit } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { Project } from '../../models/project';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-show-projects',
  templateUrl: './show-projects.component.html',
  styleUrls: ['./show-projects.component.css']
})
export class ShowProjectsComponent implements OnInit, OnDestroy {

  constructor(private projectService: ProjectService, private router: Router) { }

  projectList: Project[] = [];
  private subscriptioions = new Subscription();
  public project: any;

  ngOnInit(): void {
    this.refreshProjects();
  }

  addClick(): void {
    this.project = new Project(0, 0, 0, "", "", new Date(), new Date());
    this.router.navigateByUrl('EditProject', { state: new Project(0, 0, 0, "", "", new Date(), new Date()) });
    this.refreshProjects();
  }

  editClick(item: any) {
    this.project = item;
    this.router.navigateByUrl("EditProject", { state: this.project });
    this.refreshProjects();
  }

  deleteClick(item: any) {
    if (confirm(`Dlete project with id ${item.id}?`)) {
      this.subscriptioions = this.projectService.deleteProject(<string>item.id).subscribe(res => alert(res.toString()));
      this.refreshProjects();
    }
  }

  refreshProjects(): void {
    this.subscriptioions = this.projectService.getProjects().subscribe(data =>
      this.projectList = data);
  }

  ngOnDestroy(): void {
    this.subscriptioions.unsubscribe()
  }
}