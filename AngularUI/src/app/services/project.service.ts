import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Project } from '../models/project';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  private readonly path = "/api/Projects";
  constructor(private client: HttpClient) { }

  getProjects(): Observable<any> {
    return this.client.get<any>(this.path);
  }

  createProject(project: Project) {
    return this.client.post(this.path, project);
  }

  updateProject(project: Project) {
    return this.client.put(this.path, project);
  }

  deleteProject(id: string) {
    return this.client.delete(this.path + '/' + id);
  }
}
