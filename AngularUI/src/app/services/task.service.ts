import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Task } from '../models/task';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  private readonly path = "/api/Tasks";
  constructor(private client: HttpClient) { }

  getTasks(): Observable<any> {
    return this.client.get<any>(this.path);
  }

  createTask(task: Task) {
    return this.client.post(this.path, task);
  }

  updateTask(task: Task) {
    return this.client.put(this.path, task);
  }

  deleteTask(id: string) {
    return this.client.delete(this.path + '/' + id);
  }
}
