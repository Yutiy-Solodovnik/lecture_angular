import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Team } from '../models/team';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  private readonly path = "/api/Teams";
  constructor(private client: HttpClient) { }

  getTeams(): Observable<any> {
    return this.client.get<any>(this.path);
  }

  createTeam(team: Team) {
    return this.client.post(this.path, team);
  }

  updateTeam(team: Team) {
    return this.client.put(this.path, team);
  }

  deleteTeam(id: string) {
    return this.client.delete(this.path + '/' + id);
  }
}
