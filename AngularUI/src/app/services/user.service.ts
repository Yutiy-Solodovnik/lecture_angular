import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly path = "/api/Users";
  constructor(private client: HttpClient) { }

  getUsers(): Observable<any> {
    return this.client.get<any>(this.path);
  }

  createUser(user: User) {
    return this.client.post(this.path, user);
  }

  updateUser(user: User) {
    return this.client.put(this.path, user);
  }

  deleteUser(id: string) {
    return this.client.delete(this.path + '/' + id);
  }
}
