import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { TaskService } from '../../services/task.service';
import { Task } from '../../models/task';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ComponentCanDeactivate } from 'src/app/guards/guard';
import { Observable, Subscription } from "rxjs";
import { Router } from '@angular/router';
import { formatDate } from '@angular/common'

@Component({
  selector: 'app-edit-task',
  templateUrl: './edit-task.component.html',
  styleUrls: ['./edit-task.component.css']
})
export class EditTaskComponent implements OnInit, ComponentCanDeactivate, OnDestroy {

  constructor(private taskService: TaskService, private router: Router) {
    this.task = this.router?.getCurrentNavigation()?.extras.state;
    this.modalTitle = this.task?.id == 0 ? "Add task" : "Update task";
  }

  TaskId: number = 0;
  TaskName: string = "";
  taskForm: FormGroup = new FormGroup({});
  modalTitle: string = "";
  saved: boolean = false;
  private subscriptioions = new Subscription();

  @Input() task: any = new Task(0, 0, 3, "", "", 0, new Date(), new Date());
  ngOnInit(): void {

    this.TaskId = this.task?.id;
    this.taskForm = new FormGroup({
      projectId: new FormControl(this.task?.projectId, [
        Validators.required,
        Validators.minLength(1),
        Validators.pattern("[1-9]+")]),
      performerId: new FormControl(this.task?.performerId, [
        Validators.required,
        Validators.minLength(1),
        Validators.pattern("[1-9]+")]),
      name: new FormControl(this.task?.name, [
        Validators.required,
        Validators.minLength(2),
        Validators.pattern("[a-zA-Z ]+$")]
      ),
      description: new FormControl(this.task?.description, [
        Validators.required,
        Validators.minLength(2),
        Validators.pattern("[a-zA-Z ]+$")]),
      state: new FormControl(this.task?.state, [
        Validators.required,
        Validators.minLength(1),
        Validators.pattern("[0-3]")]),
      createdAt: new FormControl(formatDate(new Date(this.task?.createdAt), 'yyyy-MM-dd', 'en'), [
        Validators.required]),
      finishedAt: new FormControl(formatDate(new Date(this.task?.finishedAt), 'yyyy-MM-dd', 'en'), [
        Validators.required]),
    });
  }

  canDeactivate(): boolean | Observable<boolean> {

    if (!this.saved) {
      return confirm("You have unsaved data. Do you want to exit?");
    }
    else {
      return true;
    }
  }

  save() {
    this.saved = true;
  }

  addTask() {
    if (this.taskForm.valid) {
      let task: Task = {
        id: this.TaskId,
        projectId: this.taskForm.controls["projectId"].value,
        performerId: this.taskForm.controls["performerId"].value,
        name: this.taskForm.controls["name"].value,
        description: this.taskForm.controls["description"].value,
        state: this.taskForm.controls["state"].value,
        createdAt: this.taskForm.controls["createdAt"].value,
        finishedAt: this.taskForm.controls["finishedAt"].value
      }
      this.subscriptioions = this.taskService.createTask(task).subscribe(res => alert(res.toString()));
      this.save();
      this.taskForm
      this.closeClick();
    }
  }

  updateTask() {
    if (this.taskForm.valid) {
      let task: Task = {
        id: this.TaskId,
        projectId: this.taskForm.controls["projectId"].value,
        performerId: this.taskForm.controls["performerId"].value,
        name: this.taskForm.controls["name"].value,
        description: this.taskForm.controls["description"].value,
        state: this.taskForm.controls["state"].value,
        createdAt: this.taskForm.controls["createdAt"].value,
        finishedAt: this.taskForm.controls["finishedAt"].value
      }
      this.subscriptioions = this.taskService.updateTask(task).subscribe(res => alert(res.toString()));
      this.save();
      this.closeClick();
    }
  }

  closeClick() {
    this.taskForm.disable();
    this.router.navigateByUrl("Tasks");
  }

  ngOnDestroy(): void {
    this.subscriptioions.unsubscribe()
  }
}

