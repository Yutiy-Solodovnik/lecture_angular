import { Component, OnDestroy, OnInit } from '@angular/core';
import { TaskService } from '../../services/task.service';
import { Task } from '../../models/task';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-show-tasks',
  templateUrl: './show-tasks.component.html',
  styleUrls: ['./show-tasks.component.css']
})
export class ShowTasksComponent implements OnInit, OnDestroy {

  constructor(private taskService: TaskService, private router: Router) { }

  taskList: Task[] = [];
  private subscriptioions = new Subscription();
  public task: Task = new Task(0, 0, 0, "", "", 0, new Date(), new Date());

  ngOnInit(): void {
    this.refreshTasks();
  }

  addClick(): void {
    this.task = new Task(0, 0, 0, "", "", 0, new Date(), new Date());
    this.router.navigateByUrl('EditTask', { state: new Task(0, 0, 0, "", "", 0, new Date(), new Date()) });
    this.refreshTasks();
  }

  editClick(item: any) {
    this.task = item;
    this.router.navigateByUrl("EditTask", { state: this.task });
    this.refreshTasks();
  }

  deleteClick(item: any) {
    if (confirm(`Dlete task with id ${item.id}?`)) {
      this.subscriptioions = this.subscriptioions = this.taskService.deleteTask(<string>item.id).subscribe(res => alert(res.toString()));
      this.refreshTasks();
    }
  }

  refreshTasks(): void {
    this.subscriptioions = this.subscriptioions = this.taskService.getTasks().subscribe(data =>
      this.taskList = data);
  }

  ngOnDestroy(): void {
    this.subscriptioions.unsubscribe()
  }
}