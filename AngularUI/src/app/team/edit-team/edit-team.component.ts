import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { TeamService } from '../../services/team.service';
import { Team } from '../../models/team';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ComponentCanDeactivate } from 'src/app/guards/guard';
import { Observable, Subscription } from "rxjs";
import { Router } from '@angular/router';
import { formatDate } from '@angular/common';


@Component({
  selector: 'app-edit-team',
  templateUrl: './edit-team.component.html',
  styleUrls: ['./edit-team.component.css']
})
export class EditTeamComponent implements OnInit, ComponentCanDeactivate, OnDestroy {

  constructor(private teamService: TeamService, private router: Router) {
    this.team = this.router?.getCurrentNavigation()?.extras.state;
    this.modalTitle = this.team?.id == 0 ? "Add team" : "Update team";
  }

  TeamId: number = 0;
  TeamName: string = "";
  teamForm: FormGroup = new FormGroup({});
  modalTitle: string = "";
  saved: boolean = false;
  private subscriptioions = new Subscription();

  @Input() team: any = new Team(0, new Date(), "");
  ngOnInit(): void {

    this.TeamId = this.team?.id;
    this.teamForm = new FormGroup({
      teamId: new FormControl(this.team?.teamId, [
        Validators.required,
        Validators.minLength(1),
        Validators.pattern("[1-9]+")]),
      createdAt: new FormControl(formatDate(new Date(this.team?.createdAt), 'yyyy-MM-dd', 'en'), [
        Validators.required]),
      name: new FormControl(this.team?.name, [
        Validators.required,
        Validators.minLength(2),
        Validators.pattern("[a-zA-Z ]+$")]
      )
    });
  }

  canDeactivate(): boolean | Observable<boolean> {

    if (!this.saved) {
      return confirm("You have unsaved data. Do you want to exit?");
    }
    else {
      return true;
    }
  }

  save() {
    this.saved = true;
  }

  addTeam() {
    if (this.teamForm.valid) {
      let team: Team = {
        id: this.TeamId,
        createdAt: this.teamForm.controls["createdAt"].value,
        name: this.teamForm.controls["name"].value
      }
      this.subscriptioions = this.teamService.createTeam(team).subscribe(res => alert(res.toString()));
      this.save();
      this.closeClick();
    }
  }

  updateTeam() {
    if (this.teamForm.valid) {
      let team: Team = {
        id: this.TeamId,
        createdAt: this.teamForm.controls["createdAt"].value,
        name: this.teamForm.controls["name"].value
      }
      this.subscriptioions = this.teamService.updateTeam(team).subscribe(res => alert(res.toString()));
      this.save();
      this.closeClick();
    }
  }

  closeClick() {
    this.router.navigateByUrl("Teams");
  }

  ngOnDestroy(): void {
    this.subscriptioions.unsubscribe()
  }
}
