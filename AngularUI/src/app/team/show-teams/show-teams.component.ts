import { Component, OnDestroy, OnInit } from '@angular/core';
import { TeamService } from '../../services/team.service';
import { Team } from '../../models/team';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-show-teams',
  templateUrl: './show-teams.component.html',
  styleUrls: ['./show-teams.component.css']
})
export class ShowTeamsComponent implements OnInit, OnDestroy {

  constructor(private teamService: TeamService, private router: Router) { }

  teamList: Team[] = [];
  private subscriptioions = new Subscription();
  public team: Team = new Team(0, new Date(), "");

  ngOnInit(): void {
    this.refreshTeams();
  }

  addClick(): void {
    this.team = new Team(0, new Date(), "");
    this.router.navigateByUrl('EditTeam', { state: new Team(0, new Date(), "") });
    this.refreshTeams();
  }

  editClick(item: any) {
    this.team = item;
    this.router.navigateByUrl("EditTeam", { state: this.team });
    this.refreshTeams();
  }

  deleteClick(item: any) {
    if (confirm(`Dlete team with id ${item.id}?`)) {
      this.subscriptioions = this.teamService.deleteTeam(<string>item.id).subscribe(res => alert(res.toString()));
      this.refreshTeams();
    }
  }

  refreshTeams(): void {
    this.subscriptioions = this.teamService.getTeams().subscribe(data =>
      this.teamList = data);
  }

  ngOnDestroy(): void {
    this.subscriptioions.unsubscribe()
  }
}