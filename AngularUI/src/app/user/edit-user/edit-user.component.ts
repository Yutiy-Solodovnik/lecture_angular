import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ComponentCanDeactivate } from 'src/app/guards/guard';
import { Observable, Subscription } from "rxjs";
import { Router } from '@angular/router';
import { formatDate } from '@angular/common';


@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit, ComponentCanDeactivate, OnDestroy {

  constructor(private userService: UserService, private router: Router) {
    this.user = this.router?.getCurrentNavigation()?.extras.state;
    this.modalTitle = this.user?.id == 0 ? "Add user" : "Update user";
  }

  UserId: number = 0;
  UserName: string = "";
  userForm: FormGroup = new FormGroup({});
  modalTitle: string = "";
  saved: boolean = false;
  private subscriptioions = new Subscription();

  @Input() user: any = new User(0, new Date(), new Date(), 3, "", "", "");
  ngOnInit(): void {

    this.UserId = this.user?.id;
    this.userForm = new FormGroup({
      teamId: new FormControl(this.user?.teamId, [
        Validators.required,
        Validators.minLength(1),
        Validators.pattern("[1-9]+")]),
      firstName: new FormControl(this.user?.firstName, [
        Validators.required,
        Validators.minLength(2),
        Validators.pattern("[a-zA-Z ]+$")]
      ),
      lastName: new FormControl(this.user?.lastName, [
        Validators.required,
        Validators.minLength(2),
        Validators.pattern("[a-zA-Z ]+$")]),
      email: new FormControl(this.user?.email, [
        Validators.required,
        Validators.email]),
      registeredAt: new FormControl(formatDate(new Date(this.user?.registeredAt), 'yyyy-MM-dd', 'en'), [
        Validators.required]),
      birthDay: new FormControl(formatDate(new Date(this.user?.birthDay), 'yyyy-MM-dd', 'en'), [
        Validators.required]),
    });
  }

  canDeactivate(): boolean | Observable<boolean> {

    if (!this.saved) {
      return confirm("You have unsaved data. Do you want to exit?");
    }
    else {
      return true;
    }
  }

  save() {
    this.saved = true;
  }

  addUser() {
    if (this.userForm.valid) {
      let user: User = {
        id: this.UserId,
        teamId: this.userForm.controls["teamId"].value,
        firstName: this.userForm.controls["firstName"].value,
        lastName: this.userForm.controls["lastName"].value,
        email: this.userForm.controls["email"].value,
        registeredAt: this.userForm.controls["registeredAt"].value,
        birthDay: this.userForm.controls["birthDay"].value
      }
      this.subscriptioions = this.userService.createUser(user).subscribe(res => alert(res.toString()));
      this.save();
      this.closeClick();
    }
  }

  updateUser() {
    let user: User = {
      id: this.UserId,
      teamId: this.userForm.controls["teamId"].value,
      firstName: this.userForm.controls["firstName"].value,
      lastName: this.userForm.controls["lastName"].value,
      email: this.userForm.controls["email"].value,
      registeredAt: this.userForm.controls["registeredAt"].value,
      birthDay: this.userForm.controls["birthDay"].value
    }
    this.subscriptioions = this.userService.updateUser(user).subscribe(res => alert(res.toString()));
    this.save();
    this.closeClick();
  }

  closeClick() {
    this.router.navigateByUrl("Users");
  }

  ngOnDestroy(): void {
    this.subscriptioions.unsubscribe()
  }
}
