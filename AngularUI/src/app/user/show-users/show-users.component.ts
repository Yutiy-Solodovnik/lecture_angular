import { Component, OnDestroy, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-show-users',
  templateUrl: './show-users.component.html',
  styleUrls: ['./show-users.component.css']
})
export class ShowUsersComponent implements OnInit, OnDestroy {

  constructor(private userService: UserService, private router: Router) { }

  userList: User[] = [];
  private subscriptioions = new Subscription();
  public user: User = new User(0, new Date(), new Date(), 3, "", "", "");

  ngOnInit(): void {
    this.refreshUsers();
  }

  addClick(): void {
    this.user = new User(0, new Date(), new Date(), 3, "", "", "");
    this.router.navigateByUrl('EditUser', { state: new User(0, new Date(), new Date(), 3, "", "", "") });
    this.refreshUsers();
  }

  editClick(item: any) {
    this.user = item;
    this.router.navigateByUrl("EditUser", { state: this.user });
    this.refreshUsers();
  }

  deleteClick(item: any) {
    if (confirm(`Dlete user with id ${item.id}?`)) {
      this.subscriptioions = this.userService.deleteUser(<string>item.id).subscribe(res => alert(res.toString()));
      this.refreshUsers();
    }
  }

  refreshUsers(): void {
    this.subscriptioions = this.userService.getUsers().subscribe(data =>
      this.userList = data);
  }

  ngOnDestroy(): void {
    this.subscriptioions.unsubscribe()
  }
}