## Requirements for UI:
The project must be built using Angular CLI   
Create navigation between pages   
Pages for listing, editing and deleting   
-Projects   
-Project Tasks   
-Team   
-Users   
Editing pages must use Forms   
Break all logic into logical modules (projects, task ...) and then use them in the root module    
Use Guard to warn the user that they have unsaved changes    
Load data in services (use RxJS library)   
Create your own directives to visually distinguish the Task status    
Create a Pipe for a date in the format "12 вересня 2021"    
## Requirements for Web Api:
1. Use the EF Core asynchronous API instead of the synchronous one (so that requests return Task or Task rather than void or T).   

2. Refactor all methods to work with Tasks (Can't use .Wait (), .WaitAll (), .Result). Ultimately, everything from controller methods to repository methods will return tasks.   

3. When building queries, whenever possible, you should use Task.WhenAll (to parallelize the selection).   

4. Repeat the same with the client application (I hope you still have it), namely:   
- All methods return tasks   
- There is no .Wait, .Result anywhere   
- Use Task.WhenAll if needed   
5. On the client, implement a deferred task using a timer (from System.Timers) that will periodically mark a random task as completed:   
- load task list   
- choose one of the tasks at random   
- send a request to the server - mark the task as completed   
- if the operation is successful, return the id of the marked task, otherwise throw an exception   

# P.S. [Path to json files that contain data for seeding DB, are listed globally]
