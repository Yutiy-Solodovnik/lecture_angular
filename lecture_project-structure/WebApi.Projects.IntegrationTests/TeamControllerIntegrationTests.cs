﻿using lecture_EF.BLL.DTO;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace WebApi.Projects.IntegrationTests
{
    public class TeamControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory>
    {
        private readonly CustomWebApplicationFactory _factory;

        public TeamControllerIntegrationTests(CustomWebApplicationFactory factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task Create_Team_With_Right_Body()
        {
            TeamDTO teamDTO = new TeamDTO()
            {
                Id = 0,
                Name = "Test",
                CreatedAt = DateTime.Parse("2020-06-15T18:48:06.062331")
            };
            string json = JsonConvert.SerializeObject(teamDTO);
            var response = await _factory.Client.PostAsync("api/Teams", new StringContent(json));
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Theory]
        [InlineData("{\"id\":1,\"createdAt\":\"2019-06-15T18:48:06.062331\"}")]
        [InlineData("{\"id\":0,\"name\":\"T\",\"createdAt\":\"2020-06-15T18:48:06.062331\"}")]
        public async Task Create_Team_With_Wrong_Body(string bodyJson)
        {
            TeamDTO teamDTO = JsonConvert.DeserializeObject<TeamDTO>(bodyJson);
            string json = JsonConvert.SerializeObject(teamDTO);
            var response = await _factory.Client.PostAsync("api/Teams", new StringContent(json));
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
