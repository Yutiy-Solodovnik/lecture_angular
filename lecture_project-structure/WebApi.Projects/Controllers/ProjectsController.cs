﻿using AutoMapper;
using lecture_EF.BLL.DTO;
using lecture_EF.BLL.Interfaces;
using lecture_EF.DAL.Entites;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Threading.Tasks;

namespace WebApi.Projects.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class ProjectsController : ControllerBase
    {
        private IProjectService _projectService;
        private readonly IMapper _mapper;
        
        public ProjectsController(IMapper mapper, IProjectService projectService)
        {
            _mapper = mapper;
            _projectService = projectService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetProject(string id)
        {
            Project project = await _projectService.GetProject(id);
            ProjectDTO projectDTO = _mapper.Map<ProjectDTO>(project);
            return Ok(projectDTO);
        }

        [HttpGet]
        public async Task<IActionResult> GetALLProjects()
        {
            IEnumerable<Project> projects = await _projectService.GetAllProjects();
            IEnumerable<ProjectDTO> projectsDTO = _mapper.Map<IEnumerable<ProjectDTO>>(projects);
            return Ok(projectsDTO);
        }

        [HttpGet("projectInfo")]
        public async Task<IActionResult> GetProjectInfo()
        {
            List<ProjectInfo> projectInfo = await _projectService.GetProjectInfo();
            List<ProjectInfoDTO> projectInfoDTO = _mapper.Map<List<ProjectInfoDTO>>(projectInfo);
            return Ok(projectInfoDTO);
        }

        [HttpPost]
        public async Task<IActionResult> CreateProject()
        {
            try
            {
                Task<string> todoJson = new StreamReader(Request.Body).ReadToEndAsync();
                ProjectDTO projectDTO = JsonConvert.DeserializeObject<ProjectDTO>(todoJson.Result);
                Project project = _mapper.Map<Project>(projectDTO);
                await _projectService.CreateProject(project);
                return Ok(project);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProject(string id)
        {
            try
            {
                await _projectService.DeleteProject(id);
                return Ok("Deleted");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public async Task<IActionResult> UpdateProject()
        {
            try
            {
                Task<string> todoJson = new StreamReader(Request.Body).ReadToEndAsync();
                ProjectDTO projectDTO = JsonConvert.DeserializeObject<ProjectDTO>(todoJson.Result);
                Project project = _mapper.Map<Project>(projectDTO);
                await _projectService.UpdateProject(project);
                return Ok("Updated"); ;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
