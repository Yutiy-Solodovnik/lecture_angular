﻿using FakeItEasy;
using lecture_EF.BLL;
using lecture_EF.BLL.Services;
using lecture_EF.DAL.Entites;
using lecture_EF.DAL.Interfaces;
using lecture_project_structure.BLL.Interfaces;
using System;
using System.Collections.Generic;
using Xunit;

namespace lecture_Testing.BLL.Tests
{
    public class UserServiceTests : IDisposable
    {
        private User _user;
        private Team _team;
        private UserService _userService;
        private IQueryBuilder _queryBuilder;
        private IUnitOfWork _unitOfWork;

        public UserServiceTests()
        {
            _unitOfWork = A.Fake<IUnitOfWork>();
            _userService = new UserService(_unitOfWork);
            _queryBuilder = A.Fake<IQueryBuilder>();
            _userService.QueryBuilder = _queryBuilder;
            _user = new User()
            {
                Id = 1,
                FirstName = "Ivan",
                LastName = "Petrov"
            };

            _team = new Team()
            {
                Id = 3,
                Name = "Test",
                CreatedAt = DateTime.Parse("2019-08-25T18:48:06.062331")
            };
        }

        #region CreatingUserTests
        [Fact]
        public void Create_Right_User_ThenCallingUsersRepository_Create()
        {
            _userService.CreateUser(_user);
            A.CallTo(() => _unitOfWork.Users.Create(_user)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void Create_Empty_User_ThenThrowNullReferenceException()
        {
            Assert.Throws<NullReferenceException>(() => _userService.CreateUser(null));
        }

        [Fact]
        public void Create_Wrong_User_ThenThrowArgumentException()
        {
            Assert.Throws<ArgumentException>(() => _userService.CreateUser(new User()
            { 
                Id = 1,
                FirstName = null,
                LastName = null
            }));

        }
        #endregion
        #region AddUserToTeam
        [Fact]
        public void Add_Wrong_UserToTeam_ThenThrowArgumentException()
        {
            Assert.Throws<ArgumentException>(() => _userService.AddUserToTeam(new User()
            {
                Id = 1,
                FirstName = "Wrong",
                LastName = "User",
                TeamId = null
            }));
        }

        [Fact]
        public void Add_Right_UserToTeam()
        {
            _unitOfWork.Teams.Create(_team);
            User user = new User()
            {
                Id = 1,
                FirstName = "Right",
                LastName = "User",
                TeamId = null
            };
            _userService.CreateUser(user);
            user.TeamId = 3;
            _userService.AddUserToTeam(user);
            A.CallTo(() => _unitOfWork.Users.Update(user)).MustHaveHappenedOnceExactly();
        }
        #endregion
        #region LinqMethodsTests
        [Fact]
        public void Get_Users_By_Name()
        {
            _userService.GetUsersByName();
            A.CallTo(() => _userService.QueryBuilder.GetUsersByName()).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void Get_User_Info_By_Right_Id()
        {
            _userService.GetUserInfo("3");
            A.CallTo(() => _userService.QueryBuilder.GetUserInfo(3)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void Get_User_Info_By_Wrong_Id_ThenThrowArgumentException()
        {
            Assert.Throws<ArgumentException>(() => _userService.GetUserInfo("id")); 
        }
        
        [Fact]
        public void Get_Tasks_In_Project_By_Right_Id()
        {
            _userService.GetTasksInProject("4");
            A.CallTo(() => _userService.QueryBuilder.GetTasksInProject(4)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void Get_Tasks_In_Project_By_Wrong_Id_ThenThrowArgumentException()
        {
            Assert.Throws<ArgumentException>(() => _userService.GetTasksInProject("id"));
        }

        [Fact]
        public void Get_Tasks_By_User_By_Right_Id()
        {
            _userService.GetTasksByUser("3");
            A.CallTo(() => _userService.QueryBuilder.GetTasksByUser(3)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void Get_Tasks_By_User_By_Wrong_Id_ThenThrowArgumentException()
        {
            Assert.Throws<ArgumentException>(() => _userService.GetTasksByUser("id"));
        }

        [Fact]
        public void Get_Finished_Tasks_By_User_Right_Id()
        {
            _userService.GetFinishedTasksByUser("3");
            A.CallTo(() => _userService.QueryBuilder.GetFinishedTasksByUser(3)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void Get_Finished_Tasks_By_User_Wrong_Id_ThenThrowArgumentException()
        {
            Assert.Throws<ArgumentException>(() => _userService.GetFinishedTasksByUser("id"));
        }
        #endregion

        #region GetUnfinishedTasksByUser
        [Fact]
        public void Get_Unfinished_Tasks_By_User_Right_Id()
        {
            _userService.GetUnfinishedTasksByUser("3");
            A.CallTo(() => _userService.QueryBuilder.GetUnfinishedTasksByUser(3)).MustHaveHappenedOnceExactly();
        }

        [Theory]
        [InlineData("")]
        [InlineData("bad")]

        public void Get_Unfinished_Tasks_By_User_Wrong_Id_ThenThrowArgumentException(string id)
        {
            Assert.Throws<ArgumentException>(() => _userService.GetUnfinishedTasksByUser(id));
        }
        #endregion
        public void Dispose()
        {
            _userService.Dispose();
        }
    }
}
