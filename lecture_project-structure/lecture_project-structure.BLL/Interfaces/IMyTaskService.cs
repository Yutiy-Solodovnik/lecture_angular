﻿using lecture_EF.DAL.Entites;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace lecture_EF.BLL.Interfaces
{
    public interface IMyTaskService
    {
        Task CreateTask(MyTask task);
        Task<IEnumerable<MyTask>> GetAllTasks();
        Task<MyTask> GetTask(string id);
        Task UpdateTask(MyTask task);
        Task DeleteTask(string id);
        Task ChangeTaskState(MyTask task);
        Task DisposeAsync();
    }
}
