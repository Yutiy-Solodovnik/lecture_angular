﻿using lecture_EF.DAL.Entites;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace lecture_EF.BLL.Interfaces
{
    public interface IProjectService
    {
        Task CreateProject(Project project);
        Task<IEnumerable<Project>> GetAllProjects();
        Task<Project> GetProject(string id);
        Task<List<ProjectInfo>> GetProjectInfo();
        Task UpdateProject(Project project);
        Task DeleteProject(string id);
        Task DisposeAsync();
    }
}
