﻿using lecture_EF.BLL.Interfaces;
using lecture_EF.DAL.Entites;
using lecture_EF.DAL.Interfaces;
using lecture_project_structure.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace lecture_EF.BLL.Services
{
    public class UserService : IUserService
    {
        public IQueryBuilder QueryBuilder { get; set; }
        IUnitOfWork Database { get; set; }

        public UserService(IUnitOfWork unitOfWork)
        {
            Database = unitOfWork;
            QueryBuilder = new QueryBuilder(unitOfWork);
        }

        public async Task CreateUser(User user)
        {
            if (user == null)
                throw new NullReferenceException("User is empty");

            List<ValidationResult> results = new List<ValidationResult>();
            ValidationContext context = new ValidationContext(user);
            string errors = null;
            
            if (!Validator.TryValidateObject(user, context, results, true))
            {
                foreach (var error in results)
                {
                    errors += error.ErrorMessage + ", ";
                }
                throw new ArgumentException(errors);
            }
            else
            {
                await Database.Users.Create(user);
            }
        }

        public async Task DeleteUser(string id)
        {
            int _id;
            if (int.TryParse(id, out _id) && await Database.Users.Read(_id) != null)
            {
                await Database.Users.Delete(_id);
            }
            else
                throw new ArgumentException("Invalid Id");
        }

        public async Task DisposeAsync()
        {
            await Database.DisposeAsync();
        }

        public async Task<IEnumerable<User>> GetAllUsers()
        {
            return await Database.Users.ReadAll();
        }

        public async Task AddUserToTeam(User user)
        {
            if (user == null)
                throw new NullReferenceException("User is empty");

            List<ValidationResult> results = new List<ValidationResult>();
            ValidationContext context = new ValidationContext(user);
            string errors = null;

            if (!Validator.TryValidateObject(user, context, results, true))
            {
                foreach (var error in results)
                {
                    errors += error.ErrorMessage + ", ";
                }
                throw new ArgumentException(errors);
            }
            else if ((user.TeamId == null))
            {
                throw new ArgumentException("Team is not exist");
            }
            else
            {
                await Database.Users.Update(user);
            }
        }
        public async Task<User> GetUser(string id)
        {
            int _id;
            if (int.TryParse(id, out _id))
                return await Database.Users.Read(_id);
            else
                throw new ArgumentException("Invalid Id");
        }


        public async Task<Dictionary<string, int>> GetTasksInProject(string id)
        {
            int _id;
            if (int.TryParse(id, out _id))
                return await QueryBuilder.GetTasksInProject(_id);
            else
                throw new ArgumentException("Invalid Id");
        }

        public async Task<List<MyTask>> GetTasksByUser(string id)
        {
            int _id;
            if (int.TryParse(id, out _id) && id != "")
            {
                return await QueryBuilder.GetTasksByUser(_id);
            }
            else
                throw new ArgumentException("Invalid Id");
        }

        public async Task<IEnumerable<MyTaskInfo>> GetFinishedTasksByUser(string id)
        {
            int _id;
            if (int.TryParse(id, out _id))
                return await QueryBuilder.GetFinishedTasksByUser(_id);
            else
                throw new ArgumentException("Invalid Id");
        }

        public async Task<List<User>> GetUsersByName()
        {
            return await QueryBuilder.GetUsersByName();
        }

        public async Task<UserInfo> GetUserInfo(string id)
        {
            int _id;
            if (int.TryParse(id, out _id))
                return await QueryBuilder.GetUserInfo(_id);
            else
                throw new ArgumentException("Invalid Id");
        }
        public async Task UpdateUser(User user)
        {
            if (user == null)
                throw new NullReferenceException("User is empty");

            List<ValidationResult> results = new List<ValidationResult>();
            ValidationContext context = new ValidationContext(user);
            string errors = null;
            
            if (!Validator.TryValidateObject(user, context, results, true))
            {
                foreach (var error in results)
                {
                    errors += error.ErrorMessage + ", ";
                }
                throw new ArgumentException(errors);
            }
            else
            {
                await Database.Users.Update(user);
            }
        }

        public async Task<List<MyTask>> GetUnfinishedTasksByUser(string id)
        {
            int _id;
            if (int.TryParse(id, out _id))
                return await QueryBuilder.GetUnfinishedTasksByUser(_id);
            else
                throw new ArgumentException("Invalid Iddd");
        }
    }
}
