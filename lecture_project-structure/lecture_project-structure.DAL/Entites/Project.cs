﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace lecture_EF.DAL.Entites
{
    public class Project
    {
        [Key]
        public int Id { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }

        [Required(ErrorMessage = "{0} is required")]
        [StringLength(50, MinimumLength = 2,
        ErrorMessage = "Name should be minimum 2 characters and a maximum of 50 characters")]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [DataType(DataType.Text)]
        public string Description { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime Deadline { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime CreatedAt { get; set; }
        public List<MyTask> Tasks { get; set; }
        public Team Team { get; set; }
        public User Author { get; set; }
        public override string ToString()
        {
            return $"{Name}|Автор: {Author}|Команда: {Team}|Описание: {Description}|Создан: {CreatedAt}|Дедлайн: {Deadline}";
        }
    }
}
