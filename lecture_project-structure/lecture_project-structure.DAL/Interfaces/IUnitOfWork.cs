﻿using lecture_EF.DAL.Entites;
using System;
using System.Threading.Tasks;

namespace lecture_EF.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<MyTask> Tasks { get; }
        IRepository<Project> Projects { get; }
        IRepository<Team> Teams { get; }
        IRepository<User> Users { get; }
        Task DisposeAsync();
        Task SaveAsync();
    }
}
