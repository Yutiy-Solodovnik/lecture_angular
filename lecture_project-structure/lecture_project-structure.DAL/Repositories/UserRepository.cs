﻿using lecture_EF.DAL.EF;
using lecture_EF.DAL.Entites;
using lecture_EF.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace lecture_EF.DAL.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private ProjectsContext db;
        public UserRepository(ProjectsContext context)
        {
            db = context;
        }
        public async Task Create(User item)
        {
            await db.Users.AddAsync(item);
            await db.SaveChangesAsync();
        }

        public async Task Delete(int id)
        { 
            db.Users.Remove(await db.Users.SingleOrDefaultAsync(x => x.Id == id));
            await db.SaveChangesAsync();
        }

        public async Task<User> Read(int id)
        {
            return await db.Users.FindAsync(id);
        }

        public async Task<IEnumerable<User>> ReadAll()
        {
            return await db.Users.ToListAsync();
        }

        public async Task Update(User item)
        {
            User user = await Read(item.Id);
            if (user != null)
            {
                user.FirstName = item.FirstName;
                user.LastName = item.LastName;
                user.Email = item.Email;
                user.BirthDay = item.BirthDay;
                user.TeamId = item.TeamId;
                user.RegisteredAt = item.RegisteredAt;
                db.Entry(user).State = EntityState.Modified;
                await db.SaveChangesAsync();
            }
        }
    }
}
